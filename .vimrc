set nocompatible
filetype off

filetype plugin indent on

au BufReadPost *.conf set syntax=dosini
au BufReadPost *.tpo set syntax=dosini
au BufReadPost *.cfg set syntax=dosini

set tabstop=4
set shiftwidth=4
set smarttab
set et

set wrap

set ai
set cin

set ruler
set showcmd
set autoindent
set showmatch
set cursorline

set showmatch 
set hlsearch
set incsearch
set ignorecase

set lz
set number

set listchars=tab:··
set list

set ffs=dos,unix,mac
set fencs=utf-8,cp1251,koi8-r,ucs-2,cp866

if has('gui')
    colorscheme darkblue 
if has('win32')
    set guifont=Lucida_Console:h12:cRUSSIAN::
else
    set guifont=Terminus\ 14
endif
endif

set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [POS=%04l,%04v]\ [LEN=%L]   

set wildmenu

if has('gui')
if has('win32')
    au GUIEnter * call libcallnr('maximize', 'Maximize', 1)
elseif has('gui_gtk2')
    au GUIEnter * :set lines=99999 columns=99999
endif
endif

map <silent> <F8>   :Explore<CR>
map <silent> <S-F8> :sp +Explore<CR>

